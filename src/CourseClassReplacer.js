import { ApplicationService } from '@themost/common';
import { ModelClassLoaderStrategy, SchemaLoaderStrategy } from '@themost/data';
import { EdmMapping, DataObject, EdmType } from '@themost/data';

class CourseClass extends DataObject {
	@EdmMapping.func('books', EdmType.CollectionOf('Book'))
	async getBooks() {
		return this.context
			.model('Book')
			.where('courseClass')
			.equal(this.getId())
			.prepare();
	}
}

export class CourseClassReplacer extends ApplicationService {
	constructor(app) {
		super(app);
	}

	apply() {
		// get schema loader
		const schemaLoader = this.getApplication()
			.getConfiguration()
			.getStrategy(SchemaLoaderStrategy);
		if (schemaLoader == null) {
			return;
		}
		// get model definition
		const model = schemaLoader.getModelDefinition('CourseClass');
		if (model == null) {
			return;
		}
		// get model class
		const loader = this.getApplication()
			.getConfiguration()
			.getStrategy(ModelClassLoaderStrategy);
		if (loader == null) {
			return;
		}
		const CourseClassBase = loader.resolve(model);
		// extend class
		CourseClassBase.prototype.getBooks = CourseClass.prototype.getBooks;
	}
}
