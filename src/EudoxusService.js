// Universis Project copyright 2020 All rights reserved


// eslint-disable-next-line no-unused-vars
const {HttpNotAcceptableError} = require("@themost/common");
const {ApplicationService, IApplication, TraceUtils} = require('@themost/common');
const {ExpressDataApplication} = require('@themost/express');
const {eudoxusRouter} = require('./eudoxusRouter');
const {StudentIdentifierMapper} = require('./studentMapper');
const {ODataModelBuilder} = require('@themost/data');
const csv = require('@fast-csv/parse');
const { StudentReplacer } = require("./StudentReplacer");
const { StudentPeriodRegistrationReplacer } = require("./StudentPeriodRegistrationReplacer");
const { CourseClassReplacer } = require("./CourseClassReplacer");
const el = require('./locales/el.json');
const LocalScopeAccessConfiguration = require('./config/scope.access.json');

/**
 * @param {Router} parent
 * @param {Router} after
 * @param {Router} insert
 */
function insertRouterAfter(parent, after, insert) {
    const afterIndex = parent.stack.findIndex( (item) => {
        return item === after;
    });
    if (afterIndex < 0) {
        throw new Error('Target router cannot be found in parent stack.');
    }
    const findIndex = parent.stack.findIndex( (item) => {
        return item === insert;
    });
    if (findIndex < 0) {
        throw new Error('Router to be inserted cannot be found in parent stack.');
    }
    // remove last router
    parent.stack.splice(findIndex, 1);
    // move up
    parent.stack.splice(afterIndex + 1, 0, insert);
}

/**
 * @param {Router} parent
 * @param {Router} before
 * @param {Router} insert
 */
function insertRouterBefore(parent, before, insert) {
    const beforeIndex = parent.stack.findIndex( (item) => {
        return item === before;
    });
    if (beforeIndex < 0) {
        throw new Error('Target router cannot be found in parent stack.');
    }
    const findIndex = parent.stack.findIndex( (item) => {
        return item === insert;
    });
    if (findIndex < 0) {
        throw new Error('Router to be inserted cannot be found in parent stack.');
    }
    // remove last router
    parent.stack.splice(findIndex, 1);
    // move up
    parent.stack.splice(beforeIndex, 0, insert);
}

class InvalidAcademicYearError extends Error {
    constructor() {
        super('Invalid academic year provided. Expected a positive integer.');
    }
}

class InvalidAcademicPeriodError extends Error {
    constructor() {
        super('Invalid academic period provided. Expected a valid id or alternateName (winter, summer).');
    }
}

class EmptyContextError extends Error {
    constructor() {
        super('The provided application context is empty.');
    }
}

class EmptyEudoxusProviderError extends Error {
    constructor() {
        super('The ServiceProvider of eudoxus cannot be found or is inaccessible. Data cannot be fetched.');
    }
}

class InvalidStudentError extends Error {
    constructor() {
        super('The provided student is empty, cannot be found or is inaccessible.');
    }
}

class InvalidStudentDataError extends Error {
    constructor() {
        super('Either the student\'s department alternativeCode or the studentIdentifier is/are empty.');
    }
}


export class EudoxusService extends ApplicationService {

    serviceProvider;
    academicPeriodMappings = new Map([['winter', 'Ximerino'], ['summer', 'Earino']]);
    /**
     * @param {ExpressDataApplication|IApplication} app
     */
    constructor(app) {
        super(app);
        // apply Student model extension
        new StudentReplacer(app).apply();
        // apply StudentPeriodRegistration model extension
        new StudentPeriodRegistrationReplacer(app).apply();
        // apply CourseClass model extension
        new CourseClassReplacer(app).apply();
        // ensure that StudentIdentifierMapper is already an application service
        const mapper = app.getService(StudentIdentifierMapper);
        if (mapper == null) {
            TraceUtils.info(`EudoxusService: StudentMapper service has not been set. Loading default student mapper (StudentIdentifierMapper)`);
            app.useService(StudentIdentifierMapper);
        } else {
            TraceUtils.info(`EudoxusService: StudentMapper service has been already set as ${mapper.constructor.name}.`);
        }
        app.container.subscribe((container) => {
            if (container) {
                // add extra scope access elements
                const scopeAccess = app.getConfiguration().getStrategy(function ScopeAccessConfiguration() { });
                if (scopeAccess != null) {
                    scopeAccess.elements.push.apply(scopeAccess.elements, LocalScopeAccessConfiguration);
                }
                // append translations
                const translateService = app.getService(function TranslateService() {});
                if (translateService) {
                    translateService.setTranslation('el', el);
                }
                // get container router
                const router = container._router;
                // find after position
                const before = router.stack.find((item) => {
                    return item.name === 'dataContextMiddleware';
                });
                // use eudoxus router
                container.use('/eudoxus', eudoxusRouter(app));
                if (before == null) {
                    // do nothing
                    return;
                }
                // get last router
                const insert = router.stack[router.stack.length - 1];
                // insert (re-index) router
                insertRouterBefore(router, before, insert);
            }
        });

        const builder = this.getApplication().getStrategy(ODataModelBuilder);
        // refresh builder
        if (builder != null) {
            // cleanup builder and wait for next call
            builder.clean(true);
            builder.initializeSync();
        }

    }

    async getBooksForAcademicYear(context, academicYear) {
        // validate context
        if (context == null) {
            throw new EmptyContextError();
        }
        // validate academicYear
        if (!(typeof academicYear === 'number'
            && Number.isInteger(academicYear)
            && academicYear > 0)) {
            throw new InvalidAcademicYearError();
        }
        // get the eudoxus service provider
        const eudoxusServiceProvider = await this.getEudoxusServiceProvider(context);
        if (eudoxusServiceProvider == null) {
            throw new EmptyEudoxusProviderError();
        }
        // let service provider handle all authorization etc
        const booksResponse = await eudoxusServiceProvider.execute({
            // set target url
            url: '/rest/sapi/institution/courses',
            // set accept header
            headers: {
                'Accept': 'text/csv'
            },
            // set query data
            data: {
                year: academicYear
            },
            // set method
            method: 'GET'
        });
        if (typeof booksResponse !== 'string') {
            throw new HttpNotAcceptableError('Invalid eudoxus service response.');
        }
        // parse csv as string from response
        const parsedResults = await new Promise((resolve, reject) => {
            const results = [];
            csv.parseString(booksResponse, { headers: true, delimiter: ';' })
                .on('error', (error) => {
                    return reject(error);
                })
                .on('data', (row) => {
                    results.push(row);
                })
                .on('end', () => {
                    return resolve(results);
                });
        });
        // TODO: Map data to something that the project likes
        return Promise.resolve(parsedResults);
    }


    async getBookDeliveriesForAcademicYearAndPeriod(context, academicYear, academicPeriod) {
        // validate context
        if (context == null) {
            throw new EmptyContextError();
        }
        // validate academicYear
        if (!(typeof academicYear === 'number'
            && Number.isInteger(academicYear)
            && academicYear > 0)) {
            throw new InvalidAcademicYearError();
        }
        // first, try to find period in local mappings
        let period = this.academicPeriodMappings.get(academicPeriod);
        if (period == null) {
            // find academic period
            const academicPeriodName = await context
                .model('AcademicPeriod')
                .find(academicPeriod)
                .select('alternateName')
                .value();
            period = this.academicPeriodMappings.get(academicPeriodName);
            if (period == null) {
                throw new InvalidAcademicPeriodError();
            }
        }
        // get the eudoxus service provider
        const eudoxusServiceProvider = await this.getEudoxusServiceProvider(context);
        if (eudoxusServiceProvider == null) {
            throw new EmptyEudoxusProviderError();
        }
        // let service provider handle all authorization etc
        const bookDeliveriesResponse = await eudoxusServiceProvider.execute({
            // set target url
            url: '/rest/sapi/institution/student-book-deliveries',
            // set accept header
            headers: {
                'Accept': 'text/csv'
            },
            // set query data
            data: {
                year: academicYear,
                period: period
            },
            // set method
            method: 'GET'
        });
        if (typeof bookDeliveriesResponse !== 'string') {
            throw new HttpNotAcceptableError('Invalid eudoxus service response.');
        }
        // parse csv as string from response
        const parsedResults = await new Promise((resolve, reject) => {
            const results = [];
            csv.parseString(bookDeliveriesResponse, { headers: true, delimiter: ';' })
                .on('error', (error) => {
                    return reject(error);
                })
                .on('data', (row) => {
                    results.push(row);
                })
                .on('end', () => {
                    return resolve(results);
                });
        });
        // TODO: Map data to something that the project likes
        return Promise.resolve(parsedResults);
    }

    async getBookDeliveriesForStudentForYearAndPeriod(context, student, academicYear, academicPeriod) {
        // validate context
        if (context == null) {
            throw new EmptyContextError();
        }
        // validate academicYear
        if (!(typeof academicYear === 'number'
            && Number.isInteger(academicYear)
            && academicYear > 0)) {
            throw new InvalidAcademicYearError();
        }
        // first, try to find period in local mappings
        let period = this.academicPeriodMappings.get(academicPeriod);
        if (period == null) {
            // find academic period
            const academicPeriodName = await context
                .model('AcademicPeriod')
                .find(academicPeriod)
                .select('alternateName')
                .value();
            period = this.academicPeriodMappings.get(academicPeriodName);
            if (period == null) {
                throw new InvalidAcademicPeriodError();
            }
        }
        // validate student param
        if (student == null) {
            throw new InvalidStudentError();
        }
        // find student
        const studentId = typeof student === 'object' ? student.id : student;
        // note: mapper will always exist (validated at the constructor)
        const mapper = context.getApplication().getService(StudentIdentifierMapper);
        // it is very important not to use silent at this context
        student = await context
            .model('Student')
            .where('id')
            .equal(studentId)
            .select('department/alternativeCode as departmentAlternativeCode', mapper.attribute)
            .getItem();
        // and revalidate the db student
        if (student == null) {
            throw new InvalidStudentError();
        }
        if (student.departmentAlternativeCode == null || student[mapper.attribute] == null) {
            throw new InvalidStudentDataError();
        }
        // get the eudoxus service provider
        const eudoxusServiceProvider = await this.getEudoxusServiceProvider(context);
        if (eudoxusServiceProvider == null) {
            throw new EmptyEudoxusProviderError();
        }
        // let service provider handle all authorization etc
        const bookDeliveriesForStudent = await eudoxusServiceProvider.execute({
            // set target url
            url: `/rest/sapi/statement/delivered-books-status/${student[mapper.attribute]}`,
            // set accept header
            headers: {
                'Accept': 'application/json'
            },
            // set query data
            data: {
                // TODO: Check if alternativeCode is in the right format
                // or if it needs to be split
                mineduid: student.departmentAlternativeCode,
                year: academicYear,
                period: period
            },
            // set method
            method: 'GET'
        });
        // ensure response consistancy
        if (bookDeliveriesForStudent == null) {
            return Promise.resolve({
                books: []
            });
        }
        if (!(typeof bookDeliveriesForStudent === 'object'
            && bookDeliveriesForStudent.hasOwnProperty('books')
            && Array.isArray(bookDeliveriesForStudent.books))) {
                throw new HttpNotAcceptableError('Invalid eudoxus service response.');
            }
        // TODO: Map data to something that the project likes
        return Promise.resolve(bookDeliveriesForStudent);
    }

    async getEudoxusServiceProvider(context) {
        if (this.serviceProvider) {
            return this.serviceProvider;
        }
        if (context == null) {
            return;
        };
        // get eudoxus ServiceProvider as a typed item
        this.serviceProvider = await context
            .model('ServiceProvider')
            .where('alternateName')
            .equal('eudoxus')
            .getTypedItem();
        return this.serviceProvider;
    }
}

module.exports = {
    EudoxusService
}
