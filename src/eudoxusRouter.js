// Universis Project copyright 2020 All rights reserved
// eslint-disable-next-line no-unused-vars
const {ExpressDataApplication, ExpressDataContext} = require("@themost/express");
const {HttpBadRequestError, HttpForbiddenError} = require("@themost/common");
const {StudentIdentifierMapper} = require('./studentMapper');

const express = require('express');
const packageJson = require('../package.json');

/**
 * Router for Eudoxus integration
 * @param {ExpressDataApplication} app
 * @returns {Router}
 */
function eudoxusRouter(app) {
    // get whitelist
    const whitelist = app.getConfiguration().getSourceAt('settings/universis/eudoxus/whitelist') || [ '127.0.0.1' ];

    /**
     * @type {StudentIdentifierMapper}
     */
    const studentMapper = app.getService(StudentIdentifierMapper);

    if (studentMapper == null) {
        throw new Error('Invalid configuration. EudoxusService student mapper cannot be null.');
    }

    if (Array.isArray(whitelist) === false) {
        throw new Error('Invalid configuration. Service whitelist must be an array');
    }
    let router = new express.Router();
    // validate whitelisted remote address
    router.use((req, res, next) => {
        // get remote address
        const remoteAddress = req.headers['x-real-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        if (whitelist.indexOf(remoteAddress) < 0) {
            return next(new HttpForbiddenError());
        }
        return next();
    });
    // use this handler to create a router context
    router.use((req, res, next) => {
       // create router context
        const newContext = app.createContext();
        /**
         * try to find if request has already a data context
         * @type {ExpressDataContext|*}
         */
        const interactiveContext = req.context;
        // finalize already assigned context
        if (interactiveContext) {
            if (typeof interactiveContext.finalize === 'function') {
                // finalize interactive context
                return interactiveContext.finalize(() => {
                    // and assign new context
                    Object.defineProperty(req, 'context', {
                        enumerable: false,
                        configurable: true,
                        get: () => {
                            return newContext
                        }
                    });
                    // exit handler
                    return next();
                });
            }
        }
        // otherwise assign context
        Object.defineProperty(req, 'context', {
            enumerable: false,
            configurable: true,
            get: () => {
                return newContext
            }
        });
        // and exit handler
        return next();
    });
    // use this handler to finalize router context
    // important note: context finalization is very important in order
    // to close and finalize database connections, cache connections etc.
    router.use((req, res, next) => {
        req.on('end', () => {
            //on end
            if (req.context) {
                //finalize data context
                return req.context.finalize( () => {
                    //
                });
            }
        });
        return next();
    });
    router.get('/version', (req, res) => {
        return res.json({
            version: packageJson.version
        });
    });
    router.get('/getStudentStatusAndSemester/:studentIdentifier/:departmentCode',
        async (req, res, next) => {
       try {
           // get student
           const student = await req.context.model('Student')
               .where(studentMapper.attribute)
               .equal(req.params.studentIdentifier)
               .and('department/alternativeCode')
               .equal(req.params.departmentCode)
               .select(
                   studentMapper.attribute,
                   'semester',
                   'studentStatus/alternateName as studentStatus'
               ).silent()
               .getItem();
           // if student was not found
           if (student == null) {
               return res.json({
                   semester: 0,
                   status: 'student not found',
                   am: null
               });
           }
           return res.json({
               semester: student.semester,
               status: student.studentStatus === 'active' ? 'active' : 'inactive',
               am: student[studentMapper.attribute]
           });

       } catch(err) {
           return next(err);
       }
    });

    router.get('/checkCourseRegistration/:studentIdentifier/:departmentCode/:academicYear/:academicPeriod',
        async (req, res, next) => {
            try {
                // prepare courses
                const encodedCourseCodes = req.query.courseCodes;
                if (encodedCourseCodes == null) {
                    return res.json([]);
                }
                let courses = [];
                try {
                    // try to split and decode courses
                    courses = encodedCourseCodes.split(',').map((course) => {
                        return new Buffer(course, 'base64').toString();
                    });
                } catch (err) {
                    return next(new HttpBadRequestError('Bad courseCodes format. Expected an array of base-64 formatted values.', err.message));
                }
                // get student registration classes
                const items = await req.context.model('StudentCourseClass')
                    .where('registration/registrationYear').equal(req.params.academicYear)
                    .and('registration/registrationPeriod').equal(req.params.academicPeriod)
                    .and(`student/${studentMapper.attribute}`).equal(req.params.studentIdentifier)
                    .and('student/department/alternativeCode').equal(req.params.departmentCode)
                    .and('courseClass/course/displayCode').in(courses)
                    .select('courseClass/course/displayCode as courseCode')
                    .silent()
                    .getItems();
                return res.json(courses.map((courseCode) => {
                    let registration = items.findIndex((item) => {
                       return item.courseCode === courseCode;
                    }) >= 0;
                    return {
                        courseCode,
                        registration
                    }
                }));

            } catch(err) {
                return next(err);
            }
        })

    return router;
}
module.exports = {
    eudoxusRouter
}
