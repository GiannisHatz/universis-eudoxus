import { ApplicationService, Args } from '@themost/common';
import { ModelClassLoaderStrategy, SchemaLoaderStrategy } from '@themost/data';
import { EdmMapping, DataObject, EdmType } from '@themost/data';
import { EudoxusService } from './EudoxusService';

class Student extends DataObject {
	@EdmMapping.param('data', 'Object', false, true)
	@EdmMapping.action('bookDeliveries', 'Object')
	async getBookDeliveriesForYearAndPeriod(data) {
		Args.notEmpty(data);
		Args.notNegative(data.academicYear, 'Academic year');
		Args.notNegative(data.academicPeriod, 'Academic period');
		const context = this.context;
		// get eudoxus service by definition (will always exist)
		const eudoxusService = context.getApplication().getService(EudoxusService);
		// and return the book deliveries for the specified student, year and period
		return eudoxusService.getBookDeliveriesForStudentForYearAndPeriod(
			context,
			this.getId(),
			data.academicYear,
			data.academicPeriod
		);
	}
	@EdmMapping.func('books',  EdmType.CollectionOf('StudentBookRegistration'))
	getBooks() {
		return this.context.model('StudentBookRegistration').where('student').equal(this.getId()).prepare();
	}

}

export class StudentReplacer extends ApplicationService {
	constructor(app) {
		super(app);
	}

	apply() {
		// get schema loader
		const schemaLoader = this.getApplication()
			.getConfiguration()
			.getStrategy(SchemaLoaderStrategy);
		if (schemaLoader == null) {
			return;
		}
		// get model definition
		const model = schemaLoader.getModelDefinition('Student');
		if (model == null) {
			return;
		}
		// get model class
		const loader = this.getApplication()
			.getConfiguration()
			.getStrategy(ModelClassLoaderStrategy);
		if (loader == null) {
			return;
		}
		const StudentBase = loader.resolve(model);
		// extend class
		StudentBase.prototype.getBookDeliveriesForYearAndPeriod =
			Student.prototype.getBookDeliveriesForYearAndPeriod;
		StudentBase.prototype.getBooks =
			Student.prototype.getBooks;
	}
}
