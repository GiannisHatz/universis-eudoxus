import { ApplicationService } from '@themost/common';
import { SchemaLoaderStrategy } from '@themost/data';
import path from 'path';

export class StudentPeriodRegistrationReplacer extends ApplicationService {
	constructor(app) {
		super(app);
	}

	apply() {
		// get schema loader
		const schemaLoader = this.getApplication()
			.getConfiguration()
			.getStrategy(SchemaLoaderStrategy);
		// get model definition
		const model = schemaLoader.getModelDefinition('StudentPeriodRegistration');
		model.eventListeners = model.eventListeners || [];
		const bookDeliveryValidationListener = path.resolve(
			__dirname,
			'listeners/ValidateBookDeliveryBeforeClassRemoval'
		);
		const findListener = model.eventListeners.find((eventListener) => {
			return eventListener.type === bookDeliveryValidationListener;
		});
		if (findListener == null) {
			// apply listener
			model.eventListeners.push({
				type: bookDeliveryValidationListener,
			});
			// and save definition
			schemaLoader.setModelDefinition(model);
		}
	}
}
