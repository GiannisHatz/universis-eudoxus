// Universis Project copyright 2020 All rights reserved
class StudentIdentifierMapper {
    constructor() {
        //
    }
    get attribute() {
        return 'studentIdentifier';
    }
}

class UniqueIdentifierMapper extends StudentIdentifierMapper {
    get attribute() {
        return 'uniqueIdentifier';
    }
}

class StudentInstituteIdentifierMapper extends StudentIdentifierMapper {
    get attribute() {
        return 'studentInstituteIdentifier';
    }
}

module.exports = {
    StudentIdentifierMapper,
    UniqueIdentifierMapper,
    StudentInstituteIdentifierMapper
}
