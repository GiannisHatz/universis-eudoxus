# @universis/eudoxus

Universis Eudoxus is a service-to-service plugin of universis api server which provides a set of endpoints 
in order to validate student status and course registration.

## Installation

Install universis-eudoxus plugin of universis api server:

    npm i @universis/eudoxus --no-save

## Configuration

Register universis-eudoxus under universis api server `services` section of application configuration:
    
    {
        "services": [
            ...,
            {
                "serviceType": "@universis/eudoxus#EudoxusService"
            }
        ]

Add service whitelist, which is are an array of remote addresses that have the right to access endpoints,
under `settings/universis/eudoxus` section of application configuration:

     "settings": {
        "universis": {
            ...
            "eudoxus": {
                "whitelist": [
                    "127.0.0.1",
                    ...
                ]
            }
        }
     }
     
## Use Student attribute mapper service

`StudentIdentifierMapper` is a tiny service which allows `EudoxusService` to select which attribute of `Student` model is going to be used to search for a student.

There are three different student mappers:

- StudentIdentifierMapper `universis-eudoxus#StudentIdentifierMapper` which enables searching students by `studentIdentifier` attribute. This is the default mapper.
- StudentInstituteIdentifierMapper `universis-eudoxus#StudentInstituteIdentifierMapper` which enables searching students by `studentInstituteIdentifier` attribute.
- UniqueIdentifierMapper `universis-eudoxus#UniqueIdentifierMapper` which enables searching students by `uniqueIdentifier` attribute.

Use application services configuration section to set a student mapper e.g.

    {
      "services": {
        ...
        {
           "serviceType": "@universis/eudoxus#StudentIdentifierMapper",
           "strategyType": "@universis/eudoxus#StudentInstituteIdentifierMapper"
        },
        {
           "serviceType": "@universis/eudoxus#EudoxusService"
        }
      }
    }

in order to use `Student.studentInstituteIdentifier` as search attribute

or 

    {
      "services": {
        ...
        {
           "serviceType": "@universis/#StudentIdentifierMapper",
           "strategyType": "@universis/#UniqueIdentifierMapper"
        },
        {
           "type": "@universis/#EudoxusService"
        }
      }
    }

in order to use `Student.uniqueIdentifier` as search attribute.

Important note: Use `StudentIdentifierMapper` before `EudoxusService` registration.

## Validate student status

The following endpoint returns student status and semester based on the given parameters:

    /eudoxus/getStudentStatusAndSemester/:studentIdentifier/:departmentCode

e.g.

    curl --location --request GET 'http://localhost:5001/eudoxus/getStudentStatusAndSemester/100/207'
    
with response:

    {
        "semester": 38,
        "status": "active",
        "am": "100"
    }

or 

    curl --location --request GET 'http://localhost:5001/eudoxus/getStudentStatusAndSemester/0/207'
    
with response

    {
        "semester": 0,
        "status": "student not found",
        "am": null
    }
    
when the given student cannot be found.

## Validate course  registration

The following endpoint validates the registration of a given collection of courses:

    /checkCourseRegistration/:studentIdentifier/:departmentCode/:academicYear/:academicPeriod?courseCodes=courseCode1,courseCode2
    
where courseCode1,courseCode2 etc is base-64 formatted values of course codes.

e.g.

    curl --location --request GET 'http://localhost:5001/eudoxus/checkCourseRegistration/100/207/2019/1?courseCodes=zqUxNw%3D%3D%2CzqUxNg%3D%3D'
    
with response:

    [
        {
            "courseCode": "Υ17",
            "registration": true
        },
        {
            "courseCode": "Υ16",
            "registration": true
        }
    ]

where registration attribute indicates whether student has been registered for the given course or not.
